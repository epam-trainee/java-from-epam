package learn.streams.iostreamfundamentals;

import java.util.Properties;

public class PropDemo2 {
    public static void main(String[] args) {
        Properties properties = System.getProperties();
        properties.list(System.out);
    }
}
