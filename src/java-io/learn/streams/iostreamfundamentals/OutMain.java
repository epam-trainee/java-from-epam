package learn.streams.iostreamfundamentals;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class OutMain {
    public static void main(String[] args) {
        try (FileOutputStream output = new FileOutputStream("/data/out.txt", true)) {
            output.write(42);
            byte[] value = {65, 67, 100};
            output.write(value);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println();
    }
}
