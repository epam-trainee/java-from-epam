package learn.streams.iostreamfundamentals;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReaderWriterMain {
    public static void main(String[] args) {
        try (FileReader reader = new FileReader("data/info.txt");
             FileWriter writer = new FileWriter("/data/out.txt")) {
            writer.write("Java");
            int symbol = reader.read();
            writer.write(symbol);
            char[] buffer = new char[8];
            int charNum = reader.read(buffer);
            while (charNum != -1) {
                writer.write(buffer, 0, charNum);
                charNum = reader.read(buffer);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

