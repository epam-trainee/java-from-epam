package learn.streams.iostreamfundamentals;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class InputMain {
    public static void main(String[] args) {
        FileInputStream input = null;
        try {
            input = new FileInputStream("data/info.txt");
            int code = input.read();
            System.out.println(code + " char = " + (char) code);
            byte[] ar = new byte[16];
            int num = input.read(ar);
            System.out.println("num = " + num);
            System.out.println(Arrays.toString(ar));
            // input.close(); // wrong
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (input != null) {
                    try {
                        input.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}


