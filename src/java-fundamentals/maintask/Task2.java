package maintask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// Отобразить в окне консоли аргументы командной строки в обратном порядке.
public class Task2 {
    public static void main(String[] args) {
        String words = null;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            words = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(reverseString(words, words.length()-1));
    }

    private static String reverseString(String words, int index) {
        if(index == 0){
            return words.charAt(0) + "";
        }
        char letter = words.charAt(index);
        return letter + reverseString(words, index-1);
    }
}
