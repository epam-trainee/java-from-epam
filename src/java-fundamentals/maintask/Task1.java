package maintask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// Приветствовать любого пользователя при вводе его имени через командную строку.
public class Task1 {
    public static void main(String[] args) {
        String name = null;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            name = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Привет, " + name + "!");
    }
}
