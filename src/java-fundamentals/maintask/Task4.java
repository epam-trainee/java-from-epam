package maintask;

import java.util.Scanner;

// Ввести целые числа как аргументы командной строки, подсчитать их сумму (произведение) и вывести результат на консоль.
public class Task4 {
    public static void main(String[] args) {
        Scanner a = new Scanner(System.in);
        Scanner b = new Scanner(System.in);
        int num1 = a.nextInt();
        int num2 = b.nextInt();
        int sum = num1 + num2;
        int mult = num1 * num2;
        System.out.println("Сумма чисел равна " + sum);
        System.out.println("Произведение чисел равно " + mult);
    }
}
