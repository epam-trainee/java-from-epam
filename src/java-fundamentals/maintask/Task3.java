package maintask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

// Вывести заданное количество случайных чисел с переходом и без перехода на новую строку.
public class Task3 {
    public static void main(String[] args) {
        int countOfDigit = 0;
        int a;
        int b;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            countOfDigit = Integer.parseInt(reader.readLine());
        } catch (Exception e) {
            System.out.println("Вы ввели не число!");
        }
        Random random = new Random();
        for (int x = 0; x < countOfDigit; x++) {
            a = random.nextInt(countOfDigit);
            b = random.nextInt(countOfDigit);
//  System.out.println("Вывод случайных чисел с переходом на новую строку");
            System.out.print(a);
//  System.out.println("Вывод случайных чисел без перехода на новую строку");
            System.out.println(b);
        }
    }
}