package learn.main;

public class LearnMainOperator {
    public static void main(String[] args) {
        int x = 8;
        int y = 5;
        int z = x / y; // 1
        z = x % y; // 3
        final byte a = 1;
        final byte b = 2;
        byte c = a + b;
        System.out.println(c);
    }
}
