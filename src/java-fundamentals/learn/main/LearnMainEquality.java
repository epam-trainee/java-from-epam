package learn.main;

public class LearnMainEquality {
    public static void main(String[] args) {
        int count = 1;
        System.out.println(count < 1); // false
        System.out.println(count > 0); // true
        System.out.println(count <= 0); // false
        System.out.println(count >= -1); // true
        System.out.println(count == 0); // false
        System.out.println(count != 1); // false
        Object object1 = new Object();
        Object object2 = object1;
        Object object3 = null;
        System.out.println(object1 == object2); // true
        System.out.println(object3 = null); // null
    }
}
