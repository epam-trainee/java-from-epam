package learn.main;

public class LearnMainLogical {
    public static void main(String[] args) {
        int x = 1;
        int y = 1;
        boolean a = x == y; // true
        boolean b = x != y; // false
        System.out.println((x++ == y++ && x++ != y++) || x++ == y++);
        System.out.println("x= " + x + ", y= " + y);
    }
}
