package learn.main;

public class LearnMainLiteral {
    public static void main(String[] args) {
        int x = 71_010_001;
        byte b = 127;
        long k = 67101000100L;
        double d = 1.7E+2;
        float f = 5.11f;
        boolean flag = false;
        char ch = 48;
        char ch1 = 'A';
        char ch2 = '\n';
        char ch3 = '\255';
        char ch4 = '\u0000';
        String str = null;
        String str1 = "java";
        Object ob = null;
    }
}