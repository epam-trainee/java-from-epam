package learn.main;

public class LearnMainByZero {
    public static void main(String[] args) {
        double b = 0;
        double a = -1 / 0.;
        System.out.println(a); // -Infinity
        double c = Math.sqrt(-1);
        System.out.println(c); // NaN
    }
}
