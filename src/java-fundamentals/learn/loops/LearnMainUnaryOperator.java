package learn.loops;

public class LearnMainUnaryOperator {
    public static void main(String[] args) {
        int x = 1;
        x += 2; //3
        float f = 5;
        byte b = 15;
        //b = b / f;
        b /= f;
        b /= 6.8;
    }
}
