package learn.loops;

public class LearnMainTernary {
    public static void main(String[] args) {
        int a;
        int b = 10;
        int c = 5;
        final int d = 3;
        a = c > d ? b : 0;
        System.out.println(a);
    }
}
