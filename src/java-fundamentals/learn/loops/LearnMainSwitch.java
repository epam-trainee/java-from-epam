package learn.loops;

public class LearnMainSwitch {
    public static void main(String[] args) {
        byte value  = 2; // short, int, String, enum
        final byte RANGE = 127;
        switch (value) {
            case 2: // code a
            case 3: // code b
            case 4: // more code 1
                break;
            case RANGE: // more code 2
                break;
            case 0: // more code 3
                break;
            default: // more code 4
        }
    }
}
