package learn.array;

public class LearnMainArrayMulti {
    public static void main(String[] args) {
        int[][] arr = {{1, 2, 3}, {7, 8, 9, 0, 87, 77}, {42, 1, 6}};
        int x = arr[0][2];
        System.out.println(x); // 3
    }
}
