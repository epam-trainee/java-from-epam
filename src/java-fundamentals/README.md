# EPAM java-fundamentals

**Description**

Установка средств разработки, компиляции и запуска приложений. Изучение базовых типов данных и манипуляций с ними. Изучение операторов управления исполнением программы: циклов, условных переходов, операторов выбора и пр, а также их основных конструкциий и свойств. Объявление и обработка массивов данных.

**Course Goal**

Изучение основ языка Java позволит сделать первые шаги в изучении более сложных и тонких особенностей программирования. Язык прост в изучении и удобен при разработке. Начинающим программистам и программистам, переучивающимся с другого языка важно знать отличия и возможности базовых конструкций языка.

**Agenda**

Первое приложение

Ключевые слова

Типы данных

Операторы

Условные переходы

Циклы

Массивы

