package optionaltask1;// Задание. Ввести n чисел с консоли.
// Найти число, в котором количество различных цифр минимально. Если таких чисел несколько, найти первое из них.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task4 {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {

            int a = Integer.valueOf(reader.readLine());
            int b = Integer.valueOf(reader.readLine());
            int c = Integer.valueOf(reader.readLine());
            int maxDigit = Math.max(Math.max(a, b), Math.max(a, c));
            int minDigit = Math.min(Math.min(a, b), Math.min(a, c));
            if (((maxDigit - minDigit) / 2) < a)
                System.out.println("Это число больше средней длины " + a);
            if (((maxDigit - minDigit) / 2) < b)
                System.out.println("Это число больше средней длины " + b);
            if (((maxDigit - minDigit) / 2) < c)
                System.out.println("Это число больше средней длины " + c);
            System.out.println("");
            if (((maxDigit - minDigit) / 2) > a)
                System.out.println("Это число меньше средней длины " + a);
            if (((maxDigit - minDigit) / 2) > b)
                System.out.println("Это число меньше средней длины " + b);
            if (((maxDigit - minDigit) / 2) > c)
                System.out.println("Это число меньше средней длины " + c);
            System.out.println("Средняя длина " + (maxDigit - minDigit) / 2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
