package optionaltask1;// Задание. Ввести n чисел с консоли.
// Найти число, цифры в котором идут в строгом порядке возрастания. Если таких чисел несколько, найти первое из них.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Task6 {
    public static void main(String args[]) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите размер массива");
        int size = Integer.valueOf(reader.readLine());
        int[] myArray = new int[size];
        System.out.println("Введите элементы массива");

        for(int i = 0; i<size; i++) {
            myArray[i] = Integer.valueOf(reader.readLine());
        }
        for(int i = 0; i < size-1; i++) {
            for (int j = i + 1; j < myArray.length; j++) {
                if(myArray[i] > myArray[j]) {
                    int temp = myArray[i];
                    myArray[i] = myArray[j];
                    myArray[j] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(myArray));
    }
}