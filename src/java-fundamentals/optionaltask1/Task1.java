package optionaltask1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

// Задание. Ввести n чисел с консоли.
// Найти самое короткое и самое длинное число. Вывести найденные числа и их длину.

public class Task1 {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            int a = Integer.valueOf(reader.readLine());
            int b = Integer.valueOf(reader.readLine());
            int c = Integer.valueOf(reader.readLine());
            int maxDigit = Math.max(Math.max(a, b), Math.max(a, c));
            int minDigit = Math.min(Math.min(a, b), Math.min(a, c));
            System.out.println("Самое короткое число " + minDigit);
            System.out.println("Самое длинное число " + maxDigit);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}





