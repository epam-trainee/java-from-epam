package optionaltask1;// Задание. Ввести n чисел с консоли.
// Вывести числа в порядке возрастания (убывания) значений их длины.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task2 {

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            int a = Integer.valueOf(reader.readLine());
            int b = Integer.valueOf(reader.readLine());
            int c = Integer.valueOf(reader.readLine());
            if (b > c) {
                int x = b;
                b = c;
                c = x;
            }
            assert b < c;
            if (a > c) {
                int x = a;
                a = c;
                c = x;
            }
            assert a < c;
            if (a > b) {
                int x = a;
                a = b;
                b = x;
            }
            assert a < b && b < c;
            System.out.println("Возрастающая последовательность: " + a + " " + b + " " + c);
            assert a > b && b > c;
            System.out.println("Убывающая последовательность: " + c + " " + b + " " + a);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
