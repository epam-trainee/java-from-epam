package optionaltask1;// Задание. Ввести n чисел с консоли.
// Найти количество чисел, содержащих только четные цифры, а среди оставшихся — количество чисел с равным числом четных и нечетных цифр.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task5 {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            int n = Integer.valueOf(reader.readLine());
            System.out.println("Введите количество чисел");
            int[] arrayOfDigits = new int[n];

            int countOfEvenDigit = 0;
            int countOfOddDigit = 0;

            int resultCountOfEvenDigit = 0;
            int resultCountOfOddDigit = 0;

            for (int i = 0; i < n; i++) {
                System.out.println("Число " + (i + 1) + " : ");
                arrayOfDigits[i] = Integer.parseInt(reader.readLine());
                ArrayList<Integer> digits = new ArrayList<>();
                while (arrayOfDigits[i] != 0) {
                    digits.add(arrayOfDigits[i] % 10);
                    arrayOfDigits[i] /= 10;
                }
                for (int j = 0; j < digits.size(); j++) {
                    if (digits.get(j) % 2 == 0) countOfEvenDigit++;
                    else countOfOddDigit++;
                }
                if (countOfEvenDigit == digits.size()) resultCountOfEvenDigit++;
                else if (countOfEvenDigit == countOfOddDigit)
                    resultCountOfOddDigit++;

                countOfEvenDigit = 0;
                countOfOddDigit = 0;
            }

            System.out.println("Цифры состоящие из четных чисел : " + resultCountOfEvenDigit + " Числа, где кол-во четных и нечетных чисел равны : " + resultCountOfOddDigit);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


