package learn.function;

@FunctionalInterface
public interface ShapeService {
    double service(double... param);
}
