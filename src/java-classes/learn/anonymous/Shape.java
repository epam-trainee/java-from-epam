package learn.anonymous;

import java.util.StringJoiner;

public enum Shape {
    RECTANGLE{    public double computeSquare(){
      return this.a*this.b;
    }}, TRIANGLE{
        @Override
        public double computeSquare() {
            return 0;
        }
    };
    double a;
    double b;

    public void setShape(double a, double b) {
        if (a <= 0 || b <= 0) {
            throw new IllegalArgumentException();
        }
        this.a = a;
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public abstract double computeSquare();
    @Override
    public String toString() {
        return new StringJoiner(", ", Shape.class.getSimpleName() + "[", "]")
                .add("a=" + a)
                .add("b=" + b)
                .toString();
    }

}
