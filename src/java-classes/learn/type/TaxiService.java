package learn.type;

public enum TaxiService {
    ALMAZ(7788), STOLITSA(135), VIP(107), CHRYSTAL(7778), TAXY_CARGO(163);
    private int phone;

    TaxiService(int phone) {
        this.phone = phone;
    }
}
