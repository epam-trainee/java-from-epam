package learn.type;

public class Voucher {
    private int id;
    private VoucherType type;

    public Voucher(int id, VoucherType type) {
        this.id = id;
        this.type = type;
    }

    public Voucher(VoucherType type) {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public VoucherType getType() {
        return type;
    }

    public void setType(VoucherType type) {
        this.type = type;
    }
}
