package learn.type;

public class Runner {
    public static void main(String[] args) {
        MusicType type = MusicType.CLASSIC;
        type.info();
        MusicType type1 = MusicType.valueOf("ROCK");
        System.out.println(type.compareTo(type1));
    }
}
