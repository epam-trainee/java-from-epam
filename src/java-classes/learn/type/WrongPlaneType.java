package learn.type;

public enum WrongPlaneType {
    AIRBUS_A320, AIRBUS_A380, AIRBUS_A330, BOEING_737_800, BOEING_777
}
