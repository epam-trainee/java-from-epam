package learn.inner;

public class DumbOwner {
    private int id;
    private static int coefficient;

    public class DumberInner {
        private int id;

        public void setId(int id) {
            this.id = id + DumbOwner.this.id * coefficient;
        }
    }
}
