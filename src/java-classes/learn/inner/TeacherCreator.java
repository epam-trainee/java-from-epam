package learn.inner;

public class TeacherCreator {
    public static AbstractTeacher createTeacher(int id) {
        // inner local class
        class Rector extends AbstractTeacher {
            Rector(int id) {
                super(id);
            }

            @Override
            public boolean excludeStudent(Student student) {
                if (student != null) {
                    // more code
                    return true;
                } else {
                    return false;
                }
            }
        } // end of inner local class
        if (isRectorId(id)) {
            return new Rector(id);
        } else {
            return new Teacher(id);
        }
    }

    private static boolean isRectorId(int id) {
        // id checking
        return (id == 6); // stub
    }
}
