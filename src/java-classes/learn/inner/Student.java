package learn.inner;

import java.io.Serializable;

public class Student {
    private int studentId;
    private String name;
    private int group;
    private String faculty;
    private Address address;

    public class Address {
        private String city;
        private String street;
        private int houseId;
        private int flatId;
        private String email;
        private String skype;
        private long phoneNumber;

        public Address() {
        }

        public void action() {
            group = 101;
        }
    }

    public Student() {
    }

    public void operation() {
        address.city = "Kiev";
    }

    public Address getAddress() {
        return address;
    }
}
