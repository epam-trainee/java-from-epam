package advanced;

import static java.lang.Math.*;
import static java.lang.System.out;

public class LearnMainImportStatic {
    public static void main(String[] args) {
        double result = cos(2 * PI) + sin(3 * PI);
        out.println(result);
    }
}
