package advanced;

import java.util.Random;

public class Point2D {
    final int MAX_RANGE;
    private int x;
    private int y;

    {
        MAX_RANGE = new Random().nextInt(10_000);
    }

    public Point2D() {
        // MAX_RANGE = 1_000;
    }

    public Point2D(int x, int y) {
        this.x = x;
        this.y = y;
        // MAX_RANGE = new Random().nextInt(10_000);
    }
}
