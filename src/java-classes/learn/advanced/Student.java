package advanced;

public class Student {
    static {
        faculty = "MMF";
    }

    static String faculty;
    private long studentId;
    private String name = "";
    private int yearOfStudy;

    public Student(long studentId) {
        this.studentId = studentId;
    }

    public static void convertFaculty(String head) {
        System.out.println(head + " " + faculty.toLowerCase());
    }
}
