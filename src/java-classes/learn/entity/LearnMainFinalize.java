package learn.entity;

class A {
}

class B extends A {
}

class C extends B {
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}

public class LearnMainFinalize {
    public static void main(String[] args) {
        C c = new C();
        c = null;
        Runtime.getRuntime().gc();
    }
}
