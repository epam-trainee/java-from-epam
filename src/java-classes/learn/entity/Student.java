package learn.entity;

import java.util.Objects;

public class Student extends Person {
    private int id;
    private String name = "";
    private int yearOfStudy;

    public Student(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public Student(int id, int yearOfStudy) {
        this(yearOfStudy);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id && yearOfStudy == student.yearOfStudy && Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, yearOfStudy);
    }
}
