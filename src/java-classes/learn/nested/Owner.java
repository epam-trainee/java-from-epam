package learn.nested;

import java.util.Comparator;

public class Owner {
    private int value = 1;
    static int statValue = 2;

    public static class valueComparator implements Comparator<Owner> {

        @Override
        public int compare(Owner o1, Owner o2) {
            return o1.value - o2.value;
        }
    }
}
