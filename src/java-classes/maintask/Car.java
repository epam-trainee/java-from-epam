package maintask;

//Car: id, Марка, Модель, Год выпуска, Цвет, Цена, Регистрационный номер.
//
//        Создать массив объектов. Вывести:
//
//        a) список автомобилей заданной марки;
//
//        b) список автомобилей заданной модели, которые эксплуатируются больше n лет;
//
//        c) список автомобилей заданного года выпуска, цена которых больше указанной.

import java.util.ArrayList;
import java.util.List;

public class Car {
    private int carId;
    private String carBrand;
    private String carModel;
    private int carYearOfProduction;
    private String carColor;
    private int carPrice;
    private int registrationNumber;

    public Car(int carId, String carBrand, String carModel, int carYearOfProduction, String carColor, int carPrice, int registrationNumber) {
        this.carId = carId;
        this.carBrand = carBrand;
        this.carModel = carModel;
        this.carYearOfProduction = carYearOfProduction;
        this.carColor = carColor;
        this.carPrice = carPrice;
        this.registrationNumber = registrationNumber;
    }

    public static List<Car> createCar() {
        List<Car> list = new ArrayList<>();
        list.add(new Car(1, "BMW", "X5", 2010, "black", 5_000_000, 777));
        list.add(new Car(2, "Mersedes-Benz", "G-класс", 1985, "black", 7_000_000, 757));
        list.add(new Car(3, "Mazda", "X6", 2019, "grey", 4_000_000, 545));
        list.add(new Car(4, "Mazda", "X6", 2019, "red", 4_900_000, 446));
        return list;
    }

    public static void main(String[] args) {
        List<Car> list = createCar();
        List<Car> listCarByCarBrand = filterlistCarByCarBrand(list, "Mersedes-Benz");
        System.out.println("Список автомобилей заданной марки: " + listCarByCarBrand);
        List<Car> listCarByCarBrandAndYearOfProduction = filterlistCarByCarBrandAndYearOfProduction(list, "Mazda", 2019);
        System.out.println("Список автомобилей заданной модели, которые эксплуатируются более 1 года: " + listCarByCarBrandAndYearOfProduction);
        List<Car> listCarByYearOfProductionByCarPrice = filterlistCarByYearOfProductionByCarPrice(list, 2019, 4_500_000);
        System.out.println("Список автомобилей заданного года выпуска (2019), цена которых больше указанной (больше 4_000_000): " + listCarByYearOfProductionByCarPrice);
    }

    // 1
    static List<Car> filterlistCarByCarBrand(List<Car> cars, String carBrand) {
        List<Car> resultCarByCarBrand = new ArrayList<>();
        for (Car c : cars) {
            if (c.carBrand.equals(carBrand)) {
                resultCarByCarBrand.add(c);
            }
        }
        return resultCarByCarBrand;
    }

    // 2
    static List<Car> filterlistCarByCarBrandAndYearOfProduction(List<Car> cars, String carBrand, int carYearOfProduction) {
        List<Car> resultlistCarByCarBrandAndYearOfProduction = new ArrayList<>();
        for (Car f : cars) {
            if ((f.carBrand.equals(carBrand)) && (carYearOfProduction - f.carYearOfProduction) < 1) {
                resultlistCarByCarBrandAndYearOfProduction.add(f);
            }
        }
        return resultlistCarByCarBrandAndYearOfProduction;
    }

    static List<Car> filterlistCarByYearOfProductionByCarPrice(List<Car> cars, int carYearOfProduction, int carPrice) {
        List<Car> resultlistCarByYearOfProductionByCarPrice = new ArrayList<>();
        for (Car v : cars) {
            if ((v.carYearOfProduction == carYearOfProduction) && (v.carPrice > carPrice)) {
                resultlistCarByYearOfProductionByCarPrice.add(v);
            }
        }
        return resultlistCarByYearOfProductionByCarPrice;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public int getCarYearOfProduction() {
        return carYearOfProduction;
    }

    public void setCarYearOfProduction(int carYearOfProduction) {
        this.carYearOfProduction = carYearOfProduction;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public int getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(int carPrice) {
        this.carPrice = carPrice;
    }

    public int getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(int registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carId=" + carId +
                ", carBrand='" + carBrand + '\'' +
                ", carModel='" + carModel + '\'' +
                ", carYearOfProduction=" + carYearOfProduction +
                ", carColor='" + carColor + '\'' +
                ", carPrice=" + carPrice +
                ", registrationNumber=" + registrationNumber +
                '}';
    }
}
