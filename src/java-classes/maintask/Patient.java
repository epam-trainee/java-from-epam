package maintask;

import java.util.ArrayList;
import java.util.List;

// Patient: id, Фамилия, Имя, Отчество, Адрес, Телефон, Номер медицинской карты, Диагноз.
//
// Создать массив объектов. Вывести:
//
// a) список пациентов, имеющих данный диагноз;
//
// b) список пациентов, номер медицинской карты которых находится в заданном интервале.
public class Patient {
    private int id;
    private String surName;
    private String name;
    private String middleName;
    private String address;
    private int phone;
    private int numberMedicalCard;
    private String diagnosis;

    public static List<Patient> createPatient() {
        List<Patient> list = new ArrayList<>();
        list.add(new Patient(1, "Петров", "Александр", "Сергеевич", "Беларусь", 007, 10000, "Насморк"));
        list.add(new Patient(10, "Кирос", "Ульяна", "Анатольевна", "Россия", 777, 20000, "Недосып"));
        list.add(new Patient(7, "Овечкин", "Алексей", "Георгиевич", "Новая Зеландия", 333, 30000, "Насморк"));
        return list;
    }

    public static void main(String[] args) {
        List<Patient> list = createPatient();
        List<Patient> listOfPatientsByDiagnosis = filterlistOfPatientsByDiagnosis(list, "Насморк");
        System.out.println("Список пациентов, имеющих данный диагноз: " + listOfPatientsByDiagnosis);
        List<Patient> listOfPatientsByMedicalCard = filterPatientsByMedicalCard(list, 18000);
        System.out.println("Список пациентов, номер медицинской карты которых находится в заданном интервале: " + listOfPatientsByMedicalCard);
    }

    static List<Patient> filterlistOfPatientsByDiagnosis(List<Patient> patients, String diagnosis) {
        List<Patient> resultListOfPatientsByDiagnosis = new ArrayList<>();
        for (Patient h : patients) {
            if (h.diagnosis.equals(diagnosis)) {
                resultListOfPatientsByDiagnosis.add(h);
            }
        }
        return resultListOfPatientsByDiagnosis;
    }

    static List<Patient> filterPatientsByMedicalCard(List<Patient> patients, int numberMedicalCard) {
        List<Patient> resultPatientsByMedicalCard = new ArrayList<>();
        for (Patient p : patients) {
            if (p.numberMedicalCard > numberMedicalCard) {
                resultPatientsByMedicalCard.add(p);
            }
        }
        return resultPatientsByMedicalCard;
    }

    public Patient(int id, String surName, String name, String middleName, String address, int phone, int numberMedicalCard, String diagnosis) {
        this.id = id;
        this.surName = surName;
        this.name = name;
        this.middleName = middleName;
        this.address = address;
        this.phone = phone;
        this.numberMedicalCard = numberMedicalCard;
        this.diagnosis = diagnosis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public int getNumberMedicalCard() {
        return numberMedicalCard;
    }

    public void setNumberMedicalCard(int numberMedicalCard) {
        this.numberMedicalCard = numberMedicalCard;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", surName='" + surName + '\'' +
                ", name='" + name + '\'' +
                ", middleName='" + middleName + '\'' +
                ", address='" + address + '\'' +
                ", phone=" + phone +
                ", numberMedicalCard=" + numberMedicalCard +
                ", diagnosis='" + diagnosis + '\'' +
                '}';
    }
}
