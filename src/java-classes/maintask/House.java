package maintask;

// House: id, Номер квартиры, Площадь, Этаж, Количество комнат, Улица, Тип здания, Срок эксплуатации.
//
// Создать массив объектов. Вывести:
//
// a) список квартир, имеющих заданное число комнат;
//
// b) список квартир, имеющих заданное число комнат и расположенных на этаже, который находится в заданном промежутке;
//
// c) список квартир, имеющих площадь, превосходящую заданную.

import java.util.ArrayList;
import java.util.List;

public class House {
    private int id;
    private int numberOfFlat;
    private int area;
    private int floor;
    private int countOfRoom;
    private String street;
    private String typeOfBuilding;
    private int lifetime;

    public House(int id, int numberOfFlat, int area, int floor, int countOfRoom, String street, String typeOfBuilding, int lifetime) {
        this.id = id;
        this.numberOfFlat = numberOfFlat;
        this.area = area;
        this.floor = floor;
        this.countOfRoom = countOfRoom;
        this.street = street;
        this.typeOfBuilding = typeOfBuilding;
        this.lifetime = lifetime;
    }

    public static List<House> createHouse() {
        List<House> list = new ArrayList<>();
        list.add(new House(1, 620, 700, 23, 2100, "Рубинштейна", "Кирпично-монолитный", 100));
        list.add(new House(2, 720, 900, 18, 2500, "Победы", "Панельный", 50));
        list.add(new House(3, 520, 800, 20, 1800, "Руднева", "Панельный", 50));
        return list;
    }

    public static void main(String[] args) {
        List<House> list = createHouse();
        List<House> listByCountOfRoom = filterlistByCountOfRoom(list, 2000);
        System.out.println("Список квартир имеющих заданное число комнат: " + listByCountOfRoom);
        List<House> listByCountOfRoomByFloor = filterlistByCountOfRoomByFloor(list, 2000, 20);
        System.out.println("Список квартир, имеющих заданное число комнат и расположенных на этаже, который находится в заданном промежутке: " + listByCountOfRoomByFloor);
        List<House> listByArea = filterlistByArea(list, 800);
        System.out.println("Список квартир, имеющих площадь, превосходящую заданную: " + listByArea);
    }

    // 1
    static List<House> filterlistByCountOfRoom(List<House> houses, int countOfRoom) {
        List<House> resultListByCountOfRoom = new ArrayList<>();
        for (House h : houses) {
            if (h.countOfRoom > countOfRoom) {
                resultListByCountOfRoom.add(h);
            }
        }
        return resultListByCountOfRoom;
    }

    // 2
    static List<House> filterlistByCountOfRoomByFloor(List<House> houses, int countOfRoom, int floor) {
        List<House> resultlistByCountOfRoomByFloor = new ArrayList<>();
        for (House j : houses) {
            if (j.floor > floor) {
                resultlistByCountOfRoomByFloor.add(j);
            }
        }
        return resultlistByCountOfRoomByFloor;
    }

    // 3
    static List<House> filterlistByArea(List<House> houses, int area) {
        List<House> resultListByArea = new ArrayList<>();
        for (House l : houses) {
            if (l.area > area) {
                resultListByArea.add(l);
            }
        }
        return resultListByArea;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumberOfFlat() {
        return numberOfFlat;
    }

    public void setNumberOfFlat(int numberOfFlat) {
        this.numberOfFlat = numberOfFlat;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getCountOfRoom() {
        return countOfRoom;
    }

    public void setCountOfRoom(int countOfRoom) {
        this.countOfRoom = countOfRoom;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTypeOfBuilding() {
        return typeOfBuilding;
    }

    public void setTypeOfBuilding(String typeOfBuilding) {
        this.typeOfBuilding = typeOfBuilding;
    }

    public int getLifetime() {
        return lifetime;
    }

    public void setLifetime(int lifetime) {
        this.lifetime = lifetime;
    }

    @Override
    public String toString() {
        return "House{" +
                "id=" + id +
                ", numberOfFlat=" + numberOfFlat +
                ", area=" + area +
                ", floor=" + floor +
                ", countOfRoom=" + countOfRoom +
                ", street='" + street + '\'' +
                ", typeOfBuilding='" + typeOfBuilding + '\'' +
                ", lifetime=" + lifetime +
                '}';
    }
}
