package maintask;

// Создать классы, спецификации которых приведены ниже. Определить конструкторы и методы setТип(), getТип(), toString(). Определить дополнительно методы в классе, создающем массив объектов. Задать критерий выбора данных и вывести эти данные на консоль. В каждом классе, обладающем информацией, должно быть объявлено несколько конструкторов.

// Customer: id, Фамилия, Имя, Отчество, Адрес, Номер кредитной карточки, Номер банковского счета.
//
// Создать массив объектов. Вывести:
//
// a) список покупателей в алфавитно порядке;м
//
// b) список покупателей, у которых номер кредитной карточки находится в заданном интервале.

import java.util.*;

public class Customer implements Comparable<Customer> {
    private int id;
    private String surName;
    private String name;
    private String middleName;
    private String address;
    private int numberCreditCard;
    private int numberBankAccount;

    public Customer(int id, String surName, String name, String middleName, String address, int numberCreditCard, int numberBankAccount) {
        this.id = id;
        this.surName = surName;
        this.name = name;
        this.middleName = middleName;
        this.address = address;
        this.numberCreditCard = numberCreditCard;
        this.numberBankAccount = numberBankAccount;
    }

    private static List<Customer> createCustomer() {
        List<Customer> list = new ArrayList<>();
        list.add(new Customer(1, "Крылов", "Евгений", "Сергеевич", "Россия", 1111, 1234567890));
        list.add(new Customer(2, "Иванов", "Дмитрий", "Петрович", "США", 2222, 684168684));
        list.add(new Customer(3, "Смирнов", "Василий", "Борисович", "Норвегия", 3333, 75443468));
        return list;
    }

    public static void main(String[] args) {
        List<Customer> list = createCustomer();
        // a) список покупателей в алфавитном порядке
        Collections.sort(list);
        System.out.println("Список покупателей в алфавитном порядке: " + list);
        // б) список покупателей, у которых номер кредитной карточки находится в заданном интервале
        List<Customer> customerByNumberCreditCard = filterCustomerByNumberCreditCard(list, 2300);
        System.out.println("Список покупателей, у которых номер кредитной карточки находится в заданном интервале: " + customerByNumberCreditCard);
    }
    static List<Customer> filterCustomerByNumberCreditCard(List<Customer> customers, int numberCreditCard) {
        List<Customer> resultCustomerByNumberCreditCard = new ArrayList<>();
        for (Customer d : customers) {
            if (d.numberCreditCard > numberCreditCard) {
                resultCustomerByNumberCreditCard.add(d);
            }
        }
        return resultCustomerByNumberCreditCard;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumberCreditCard() {
        return numberCreditCard;
    }

    public void setNumberCreditCard(int numberCreditCard) {
        this.numberCreditCard = numberCreditCard;
    }

    public int getNumberBankAccount() {
        return numberBankAccount;
    }

    public void setNumberBankAccount(int numberBankAccount) {
        this.numberBankAccount = numberBankAccount;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", surName='" + surName + '\'' +
                ", name='" + name + '\'' +
                ", middleName='" + middleName + '\'' +
                ", address='" + address + '\'' +
                ", numberCreditCard=" + numberCreditCard +
                ", numberBankAccount=" + numberBankAccount +
                '}';
    }


    @Override
    public int compareTo(Customer o) {
        return name.compareTo(o.getName());
    }
}

