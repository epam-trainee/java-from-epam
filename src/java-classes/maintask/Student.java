package maintask;

// Создать классы, спецификации которых приведены ниже. Определить конструкторы и методы setТип(), getТип(), toString(). Определить дополнительно методы в классе, создающем массив объектов. Задать критерий выбора данных и вывести эти данные на консоль. В каждом классе, обладающем информацией, должно быть объявлено несколько конструкторов.
// Student: id, Фамилия, Имя, Отчество, Дата рождения, Адрес, Телефон, Факультет, Курс, Группа.
// Создать массив объектов. Вывести:
// a) список студентов заданного факультета;
// b) списки студентов для каждого факультета и курса;
// c) список студентов, родившихся после заданного года;
// d) список учебной группы.

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

class Student {
    private int id;
    private String surname;
    private String name;
    private String middleName;
    private LocalDate dateOfBirth;
    private String address;
    private String phone;
    private String faculty;
    private int course;
    private int group;

    public Student(int id, String surName, String name, String middleName, LocalDate dateOfBirth, String address, String phone, String faculty, int course, int group) {
        this.id = id;
        this.surname = surName;
        this.name = name;
        this.middleName = middleName;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.phone = phone;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
    }

    public static void main(String[] args) {
        List<Student> list = createTestStudents();
        List<Student> studentsByFaculty = filterStudentsByFaculty(list, "Информатика");
        System.out.println("Cписок студентов заданного факультета (Информатика): " + studentsByFaculty);

        List<Student> studentsByFacultyAndCourse = filterstudentsByFacultyAndCourse(list, "Информатика", 2);
        System.out.println("Cписки студентов для каждого факультета (Информатика) и курса (2): " + studentsByFacultyAndCourse);

        LocalDate date = LocalDate.of(1980, 10, 5);
        List<Student> studentsAfterYear = filterStudentsAfterYear(list, date);
        System.out.println("Cписок студентов, родившихся после заданного года (1980-10-05): " + studentsAfterYear);

        List<Student> studentsCourseNumber = filterStudentsCourseNumber(list, 342);
        System.out.println("Список учебной группы (342): " + studentsCourseNumber);
    }

    private static List<Student> createTestStudents() {
        List<Student> list = new ArrayList<>();
        list.add(new Student(1, "Иванов", "Иван", "Иванович", LocalDate.of(2000, 12, 20), "Россия", "11111", "ИЗО", 1, 442));
        list.add(new Student(2, "Романов", "Рома", "Романович", LocalDate.of(1986, 12, 20), "Россия", "11111", "Русский", 2, 242));
        list.add(new Student(4, "Петров", "Петя", "Петрович", LocalDate.of(1990, 12, 20), "США", "11111", "Информатика", 3, 342));
        list.add(new Student(3, "Васев", "Вася", "Васевич", LocalDate.of(1980, 12, 20), "США", "11111", "Информатика", 2, 342));
        list.add(new Student(5, "Валерьянов", "Валера", "Валерьянович", LocalDate.of(1970, 12, 20), "Россия", "11111", "Информатика", 2, 442));
        return list;
    }

    // 1
    static List<Student> filterStudentsByFaculty(List<Student> students, String faculty) {
        List<Student> resultFaculty = new ArrayList<>();
        for (Student c : students) {
            // c.faculty - свойство объекта c, которое содержит значение "Информатика"
            if (c.faculty.equals(faculty)) {
                resultFaculty.add(c);
            }
        }
        return resultFaculty;
    }

    // 2
    static List<Student> filterstudentsByFacultyAndCourse(List<Student> students, String faculty, int course) {
        List<Student> resultFacultyAndCourse = new ArrayList<>();
        for (Student k : students) {
            if (k.faculty.equals(faculty) && k.course == course) {
                resultFacultyAndCourse.add(k);
            }
        }
        return resultFacultyAndCourse;
    }

    // 3
    static List<Student> filterStudentsAfterYear(List<Student> students, LocalDate dateOfBirth) {
        List<Student> resultStudentsAfterYear = new ArrayList<>();
        for (Student m : students) {
            if (m.dateOfBirth.isAfter(dateOfBirth)) {
                resultStudentsAfterYear.add(m);
            }
        }
        return resultStudentsAfterYear;
    }

    // 4
    static List<Student> filterStudentsCourseNumber(List<Student> students, int group) {
        List<Student> resultStudentsCourseNumber = new ArrayList<>();
        for (Student t : students) {
            if (t.group == group) {
                resultStudentsCourseNumber.add(t);
            }
        }
        return resultStudentsCourseNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", middleName='" + middleName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", faculty='" + faculty + '\'' +
                ", course=" + course +
                ", group=" + group +
                '}';
    }
}


