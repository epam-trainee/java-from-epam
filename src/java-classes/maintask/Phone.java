package maintask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// Phone: id, Фамилия, Имя, Отчество, Адрес, Номер кредитной карточки, Дебет, Кредит, Время городских и междугородных разговоров.
//
// Создать массив объектов. Вывести:
//
// a) сведения об абонентах, у которых время внутригородских разговоров превышает заданное;
//
// b) сведения об абонентах, которые пользовались междугородной связью;
//
// c) сведения об абонентах в алфавитном порядке

public class Phone implements Comparable<Phone> {
    private int id;
    private String surName;
    private String name;
    private String middleName;
    private String address;
    private int numberOfCreditCard;
    private int debit;
    private int credit;
    private int timeCityCall;
    private int timeLongDistanceCall;

    public Phone(int id, String surName, String name, String middleName, String address, int numberOfCreditCard, int debit, int credit, int timeCityCall, int timeLongDistanceCall) {
        this.id = id;
        this.surName = surName;
        this.name = name;
        this.middleName = middleName;
        this.address = address;
        this.numberOfCreditCard = numberOfCreditCard;
        this.debit = debit;
        this.credit = credit;
        this.timeCityCall = timeCityCall;
        this.timeLongDistanceCall = timeLongDistanceCall;
    }

    public static List<Phone> createPhone() {
        List<Phone> list = new ArrayList<>();
        list.add(new Phone(1, "Колесников", "Дмитрий", "Владимирович", "Украина", 7777, 10000, 10000, 10, 200));
        list.add(new Phone(1, "Тимченко", "Руслан", "Анатольевич", "Россия", 3333, 1000, 100000, 40, 100));
        list.add(new Phone(1, "Борисов", "Александр", "Викторович", "Австралия", 7777, 5000, 3000, 50, 400));
        return list;
    }

    public static void main(String[] args) {
        List<Phone> list = createPhone();
        List<Phone> listByCityCall = filterlistByCityCall(list, 40);
        System.out.println("Абоненты, у которых время внутригородских которых: " + listByCityCall);
        List<Phone> listTimeLongDistanceCall = filterTimeLongDistanceCall(list);
        System.out.println("Сведения об абонентах, которые пользовались междугородной связью: " + listTimeLongDistanceCall);
        Collections.sort(list);
        System.out.println("Сведения об абонентах в алфавитном порядке: " + list);
    }

    // 1
    static List<Phone> filterlistByCityCall(List<Phone> phones, int timeCityCall) {
        List<Phone> resultListByCityCall = new ArrayList<>();
        for (Phone ph : phones) {
            if (ph.timeCityCall > timeCityCall) {
                resultListByCityCall.add(ph);
            }
        }
        return resultListByCityCall;
    }

    // 2
    static List<Phone> filterTimeLongDistanceCall(List<Phone> phones) {
        List<Phone> resultTimeLongDistanceCall = new ArrayList<>();
        for (Phone ph : phones) {
            if (ph.timeLongDistanceCall > 0) {
                resultTimeLongDistanceCall.add(ph);
            }
        }
        return resultTimeLongDistanceCall;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumberOfCreditCard() {
        return numberOfCreditCard;
    }

    public void setNumberOfCreditCard(int numberOfCreditCard) {
        this.numberOfCreditCard = numberOfCreditCard;
    }

    public int getDebit() {
        return debit;
    }

    public void setDebit(int debit) {
        this.debit = debit;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public int getTimeCityCall() {
        return timeCityCall;
    }

    public void setTimeCityCall(int timeCityCall) {
        this.timeCityCall = timeCityCall;
    }

    public int getTimeLongDistanceCall() {
        return timeLongDistanceCall;
    }

    public void setTimeLongDistanceCall(int timeLongDistanceCall) {
        this.timeLongDistanceCall = timeLongDistanceCall;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "id=" + id +
                ", surName='" + surName + '\'' +
                ", name='" + name + '\'' +
                ", middleName='" + middleName + '\'' +
                ", address='" + address + '\'' +
                ", numberOfCreditCard=" + numberOfCreditCard +
                ", debit=" + debit +
                ", credit=" + credit +
                ", timeCityCall=" + timeCityCall +
                ", timeLongDistanceCall=" + timeLongDistanceCall +
                '}';
    }

    @Override
    public int compareTo(Phone o) {
        return 0;
    }
}
