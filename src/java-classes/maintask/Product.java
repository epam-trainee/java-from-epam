package maintask;

// Product: id, Наименование, UPC, Производитель, Цена, Срок хранения, Количество.
//
// Создать массив объектов. Вывести:
//
// a) список товаров для заданного наименования;
//
// b) список товаров для заданного наименования, цена которых не превосходит заданную;
//
// c) список товаров, срок хранения которых больше заданного.

import java.util.ArrayList;
import java.util.List;

public class Product {
    private int productId;
    private String productName;
    private int UPC;
    private String productManufacturer;
    private int productPrice;
    private int productStorageLife;
    private int productCount;

    public Product(int productId, String productName, int UPC, String productManufacturer, int productPrice, int productStorageLife, int productCount) {
        this.productId = productId;
        this.productName = productName;
        this.UPC = UPC;
        this.productManufacturer = productManufacturer;
        this.productPrice = productPrice;
        this.productStorageLife = productStorageLife;
        this.productCount = productCount;
    }

    public static List<Product> createProduct() {
        List<Product> list = new ArrayList<>();
        list.add(new Product(1, "мандарин", 123456, "Абхазия", 30, 7, 1000));
        list.add(new Product(2, "апельсин", 123451, "Турция", 40, 10, 2000));
        list.add(new Product(3, "мандарин", 123452, "Турция", 25, 10, 3000));
        list.add(new Product(4, "инжир", 123453, "Египет", 70, 7, 500));
        return list;
    }

    public static void main(String[] args) {
        List<Product> list = createProduct();
        List<Product> listProductByName = filterListProductByName(list, "мандарин");
        System.out.println("Список товаров для заданного наименования(мандарин): " + listProductByName);
        List<Product> listProductByPrice = filterListProductByPrice(list, 35);
        System.out.println("Список товаров для заданного наименования, цена которых не превосходит заданную (35): " + listProductByPrice);
        List<Product> listProductByStorageLife = filterListProductByStorageLife(list, 8);
        System.out.println("Список товаров, срок хранения которых больше заданного (8 дней): " + listProductByStorageLife);
    }

    // 1
    static List<Product> filterListProductByName(List<Product> products, String productName) {
        List<Product> resultListProductByName = new ArrayList<>();
        for (Product pn : products) {
            if (pn.productName.equals(productName)) {
                resultListProductByName.add(pn);
            }
        }
        return resultListProductByName;
    }

    // 2
    static List<Product> filterListProductByPrice(List<Product> products, int productPrice) {
        List<Product> resultListProductByPrice = new ArrayList<>();
        for (Product pp : products) {
            if (pp.productPrice < productPrice) {
                resultListProductByPrice.add(pp);
            }
        }
        return resultListProductByPrice;
    }

    // 3
    static List<Product> filterListProductByStorageLife(List<Product> products, int productStorageLife) {
        List<Product> resultListProductByStorageLife = new ArrayList<>();
        for (Product psl : products) {
            if (psl.productStorageLife > productStorageLife) {
                resultListProductByStorageLife.add(psl);
            }
        }
        return resultListProductByStorageLife;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getUPC() {
        return UPC;
    }

    public void setUPC(int UPC) {
        this.UPC = UPC;
    }

    public String getProductManufacturer() {
        return productManufacturer;
    }

    public void setProductManufacturer(String productManufacturer) {
        this.productManufacturer = productManufacturer;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    public int getProductStorageLife() {
        return productStorageLife;
    }

    public void setProductStorageLife(int productStorageLife) {
        this.productStorageLife = productStorageLife;
    }

    public int getProductCount() {
        return productCount;
    }

    public void setProductCount(int productCount) {
        this.productCount = productCount;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                ", UPC=" + UPC +
                ", productManufacturer='" + productManufacturer + '\'' +
                ", productPrice=" + productPrice +
                ", productStorageLife=" + productStorageLife +
                ", productCount=" + productCount +
                '}';
    }
}
