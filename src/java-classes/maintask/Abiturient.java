package maintask;

import java.util.ArrayList;
import java.util.List;

// Abiturient: id, Фамилия, Имя, Отчество, Адрес, Телефон, Оценки.
//
// Создать массив объектов. Вывести:
//
// a) список абитуриентов, имеющих неудовлетворительные оценки;
//
// b) список абитуриентов, у которых сумма баллов выше заданной;
//
// c) выбрать заданное число n абитуриентов, имеющих самую высокую сумму баллов (вывести также полный список абитуриентов, имеющих полупроходную сумму).
public class Abiturient {
    private int id;
    private String surName;
    private String name;
    private String middleName;
    private String address;
    private int phone;
    private int mark;
    private int mark2;

    public Abiturient(int id, String surName, String name, String middleName, String address, int phone, int mark, int mark2) {
        this.id = id;
        this.surName = surName;
        this.name = name;
        this.middleName = middleName;
        this.address = address;
        this.phone = phone;
        this.mark = mark;
        this.mark2 = mark2;
    }

    public static List<Abiturient> createAbiturient() {
        List<Abiturient> list = new ArrayList<>();
        list.add(new Abiturient(1, "Кузнецов", "Юрий", "Анатольевич", "Эстония", 888, 5, 4));
        list.add(new Abiturient(2, "Смирнов", "Константин", "Алексеевич", "ЮАР", 777, 4, 3));
        list.add(new Abiturient(3, "Полстайнен", "Анастасия", "Георгиевна", "Финляндия", 666, 2, 2));
        return list;
    }

    public static void main(String[] args) {
        List<Abiturient> list = createAbiturient();
        List<Abiturient> listByBadMark = filterlistByBadMark(list, 3);
        System.out.println("Список студентов, имеющих неудовлетворительные оценки: " + listByBadMark);
        List<Abiturient> listBySum = filterlistBySum(list);
        System.out.println("Список абитуриентов, у которых сумма баллов выше заданной: " + listBySum);
        List<Abiturient> listByHighestSumOfMark = filterlistByHighestSumOfMark(list);
        System.out.println("Выбрать заданное число n абитуриентов, имеющих самую высокую сумму баллов: " + listByHighestSumOfMark);
    }

    // 1
    static List<Abiturient> filterlistByBadMark(List<Abiturient> abiturients, int mark) {
        List<Abiturient> resultlistByBadMark = new ArrayList<>();
        for (Abiturient a : abiturients) {
            if (a.mark > mark) {
                resultlistByBadMark.add(a);
            }
        }
        return resultlistByBadMark;
    }

    // 2
    static List<Abiturient> filterlistBySum(List<Abiturient> abiturients) {
        List<Abiturient> resultlistBySum = new ArrayList<>();
        for (Abiturient b : abiturients) {
            int sum = 7;
            if ((b.mark + b.mark2) > sum) {
                resultlistBySum.add(b);
            }
        }
        return resultlistBySum;
    }

    // 3
    static List<Abiturient> filterlistByHighestSumOfMark(List<Abiturient> abiturients) {
        List<Abiturient> resultlistByHighestSumOfMark = new ArrayList<>();
        int sum = 7;
        for (Abiturient c : abiturients) {
            if ((c.mark + c.mark2) > sum) {
                resultlistByHighestSumOfMark.add(c);
                System.out.println("Вывести полный список абитуриентов, имеющих полупроходную сумму: " + c);
            }
        }
        return resultlistByHighestSumOfMark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    @Override
    public String toString() {
        return "Abiturient{" +
                "id=" + id +
                ", surName='" + surName + '\'' +
                ", name='" + name + '\'' +
                ", middleName='" + middleName + '\'' +
                ", address='" + address + '\'' +
                ", phone=" + phone +
                ", mark=" + mark +
                '}';
    }
}
