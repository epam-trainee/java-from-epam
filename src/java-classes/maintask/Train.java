package maintask;

// Train: Пункт назначения, Номер поезда, Время отправления, Число мест (общих, купе, плацкарт, люкс).
//
// Создать массив объектов. Вывести:
//
// a) список поездов, следующих до заданного пункта назначения;
//
// b) список поездов, следующих до заданного пункта назначения и отправляющихся после заданного часа;
//
// c) список поездов, отправляющихся до заданного пункта назначения и имеющих общие места.

import java.util.ArrayList;
import java.util.List;

public class Train {
    private String pointOfDestination;
    private int trainNumber;
    private double departureTime;
    private int countOfTrainPlaceCommon;
    private int countOfTrainPlaceCoupe;
    private int countOfTrainPlacePlatzkart;
    private int countOfTrainPlaceLux;

    public Train(String pointOfDestination, int trainNumber, double departureTime, int countOfTrainPlaceCommon, int countOfTrainPlaceCoupe, int countOfTrainPlacePlatzkart, int countOfTrainPlaceLux) {
        this.pointOfDestination = pointOfDestination;
        this.trainNumber = trainNumber;
        this.departureTime = departureTime;
        this.countOfTrainPlaceCommon = countOfTrainPlaceCommon;
        this.countOfTrainPlaceCoupe = countOfTrainPlaceCoupe;
        this.countOfTrainPlacePlatzkart = countOfTrainPlacePlatzkart;
        this.countOfTrainPlaceLux = countOfTrainPlaceLux;
    }

    static List<Train> createTrain() {
        List<Train> list = new ArrayList<>();
        list.add(new Train("Сочи", 303, 16.30, 80, 30, 90, 10));
        list.add(new Train("Санкт-Петербург", 220, 10.00, 60, 30, 90, 10));
        list.add(new Train("Таллин", 103, 13.10, 80, 30, 90, 10));
        list.add(new Train("Санкт-Петербург", 430, 20.10, 80, 30, 90, 10));
        list.add(new Train("Санкт-Петербург", 1303, 17.05, 0, 30, 90, 10));
        return list;
    }

    public static void main(String[] args) {
        List<Train> list = createTrain();
        List<Train> listTrainByPointOfDestination = filterListTrainByPointOfDestination(list, "Санкт-Петербург");
        System.out.println("Список поездов следующих до заданного места назначения(Санкт-Петербург): " + listTrainByPointOfDestination);
        List<Train> listTrainByPointOfDestinationByTime = filterListTrainByPointOfDestinationByTime(list, "Санкт-Петербург", 12.00);
        System.out.println("Cписок поездов, следующих до заданного пункта назначения и отправляющихся после заданного часа(Санкт-Петербург, после 12.00)" + listTrainByPointOfDestinationByTime);
        List<Train> listTrainByPointOfDestinationByCommonPlace = filterListTrainByPointOfDestinationByCommonPlace(list, "Санкт-Петербург");
        System.out.println("Список поездов, отправляющихся до заданного пункта назначения и имеющих общие места: " + listTrainByPointOfDestinationByCommonPlace);
    }

    // 1
    static List<Train> filterListTrainByPointOfDestination(List<Train> trains, String pointOfDestination) {
        List<Train> resultListTrainByPointOfDestination = new ArrayList<>();
        for (Train td : trains) {
            if (td.pointOfDestination.equals(pointOfDestination)) {
                resultListTrainByPointOfDestination.add(td);
            }
        }
        return resultListTrainByPointOfDestination;
    }

    // 2
    static List<Train> filterListTrainByPointOfDestinationByTime(List<Train> trains, String pointOfDestination, double departureTime) {
        List<Train> resultListTrainByPointOfDestinationByTime = new ArrayList<>();
        for (Train k : trains) {
            if (k.pointOfDestination.equals(pointOfDestination) && k.departureTime > departureTime) {
                resultListTrainByPointOfDestinationByTime.add(k);
            }
        }
        return resultListTrainByPointOfDestinationByTime;
    }

    // 3
    static List<Train> filterListTrainByPointOfDestinationByCommonPlace(List<Train> trains, String pointOfDestination) {
        List<Train> resultListTrainByPointOfDestinationByCommonPlace = new ArrayList<>();
        for (Train s : trains) {
            if (s.pointOfDestination.equals(pointOfDestination) && s.countOfTrainPlaceCommon > 0) {
                resultListTrainByPointOfDestinationByCommonPlace.add(s);
            }
        }
        return resultListTrainByPointOfDestinationByCommonPlace;
    }

    public String getPointOfDestination() {
        return pointOfDestination;
    }

    public void setPointOfDestination(String pointOfDestination) {
        this.pointOfDestination = pointOfDestination;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(int trainNumber) {
        this.trainNumber = trainNumber;
    }

    public double getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(double departureTime) {
        this.departureTime = departureTime;
    }

    public int getCountOfTrainPlaceCommon() {
        return countOfTrainPlaceCommon;
    }

    public void setCountOfTrainPlaceCommon(int countOfTrainPlaceCommon) {
        this.countOfTrainPlaceCommon = countOfTrainPlaceCommon;
    }

    public int getCountOfTrainPlaceCoupe() {
        return countOfTrainPlaceCoupe;
    }

    public void setCountOfTrainPlaceCoupe(int countOfTrainPlaceCoupe) {
        this.countOfTrainPlaceCoupe = countOfTrainPlaceCoupe;
    }

    public int getCountOfTrainPlacePlatzkart() {
        return countOfTrainPlacePlatzkart;
    }

    public void setCountOfTrainPlacePlatzkart(int countOfTrainPlacePlatzkart) {
        this.countOfTrainPlacePlatzkart = countOfTrainPlacePlatzkart;
    }

    public int getCountOfTrainPlaceLux() {
        return countOfTrainPlaceLux;
    }

    public void setCountOfTrainPlaceLux(int countOfTrainPlaceLux) {
        this.countOfTrainPlaceLux = countOfTrainPlaceLux;
    }

    @Override
    public String toString() {
        return "Train{" +
                "pointOfDestination='" + pointOfDestination + '\'' +
                ", trainNumber=" + trainNumber +
                ", departureTime=" + departureTime +
                ", countOfTrainPlaceCommon=" + countOfTrainPlaceCommon +
                ", countOfTrainPlaceCoupe=" + countOfTrainPlaceCoupe +
                ", countOfTrainPlacePlatzkart=" + countOfTrainPlacePlatzkart +
                ", countOfTrainPlaceLux=" + countOfTrainPlaceLux +
                '}';
    }
}
