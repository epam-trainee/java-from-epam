package maintask;

import java.util.ArrayList;
import java.util.List;

// Book: id, Название, Автор (ы), Издательство, Год издания, Количество страниц, Цена, Тип переплета.
//
// Создать массив объектов. Вывести:
//
// a) список книг заданного автора;
//
// b) список книг, выпущенных заданным издательством;
//
// c) список книг, выпущенных после заданного года.
public class Book {
    private int id;
    private String name;
    private String author;
    private String publishingHouse;
    private int yearOfPublishing;
    private int countOfPages;
    private int price;
    private String bindingType;

    public Book(int id, String name, String author, String publishingHouse, int yearOfPublishing, int countOfPages, int price, String bindingType) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.publishingHouse = publishingHouse;
        this.yearOfPublishing = yearOfPublishing;
        this.countOfPages = countOfPages;
        this.price = price;
        this.bindingType = bindingType;
    }

    public static List<Book> createBook() {
        List<Book> list = new ArrayList<>();
        list.add(new Book(10, "Буратино", "А.Н.Толстой", "АСТ", 1936, 200, 10, "Мягкий переплет"));
        list.add(new Book(11, "Муми-тролли", "Туве Янссон", "ФиннПресс", 1970, 800, 20, "Твердый переплет"));
        list.add(new Book(12, "Пеппи длинный чулок", "Астрид Линдгрен", "СвиденПресс", 1980, 500, 30, "Твердый переплет"));
        list.add(new Book(12, "Приключения Эмиля из Лённеберги", "Астрид Линдгрен", "СвиденПресс", 1985, 400, 25, "Мягкий переплет"));
        return list;
    }


    public static void main(String[] args) {
        List<Book> list = createBook();
        List<Book> listBookByAuthor = filterBookByAuthor(list, "Астрид Линдгрен");
        System.out.println("Список книг заданного автора: " + listBookByAuthor);
        List<Book> listBookByPublishingHouse = filterBookByPublishingHouse(list, "ФиннПресс");
        System.out.println("Список книг выпущенных заданным издательством: " + listBookByPublishingHouse);
        List<Book> listBookByYearOfPublishing = filterBookByYearOfPublishing(list, 1975);
        System.out.println("Список книг выпущенных после заданного года: " + listBookByYearOfPublishing);
    }

    // 1
    static List<Book> filterBookByAuthor(List<Book> books, String author) {
        List<Book> resultBookByAuthor = new ArrayList<>();
        for (Book book : books) {
            if (book.author.equals(author)) {
                resultBookByAuthor.add(book);
            }
        }
        return resultBookByAuthor;
    }

    // 2
    static List<Book> filterBookByPublishingHouse(List<Book> books, String publishingHouse) {
        List<Book> resultBookByPublishingHouse = new ArrayList<>();
        for (Book book : books) {
            if (book.publishingHouse.equals(publishingHouse)) {
                resultBookByPublishingHouse.add(book);
            }
        }
        return resultBookByPublishingHouse;
    }

    // 3
    static List<Book> filterBookByYearOfPublishing(List<Book> books, int yearOfPublishing) {
        List<Book> resultYearOfPublishing = new ArrayList<>();
        for (Book g : books) {
            if (g.yearOfPublishing > yearOfPublishing) {
                resultYearOfPublishing.add(g);
            }
        }
        return resultYearOfPublishing;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public int getYearOfPublishing() {
        return yearOfPublishing;
    }

    public void setYearOfPublishing(int yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
    }

    public int getCountOfPages() {
        return countOfPages;
    }

    public void setCountOfPages(int countOfPages) {
        this.countOfPages = countOfPages;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getBindingType() {
        return bindingType;
    }

    public void setBindingType(String bindingType) {
        this.bindingType = bindingType;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", publishingHouse='" + publishingHouse + '\'' +
                ", yearOfPublishing=" + yearOfPublishing +
                ", countOfPages=" + countOfPages +
                ", price=" + price +
                ", bindingType='" + bindingType + '\'' +
                '}';
    }
}

