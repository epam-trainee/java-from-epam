package jenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericMain {
    public static void main(String[] args) {
        List<Persona> list = new ArrayList<>();
        list.add(new Student());
        //   list.add("abc"); // ошибка. Можем использовать только Person и его наследников

        list.add(new Student());
        list.add(new Persona());
        Persona persona = list.get(2);
    }
}
