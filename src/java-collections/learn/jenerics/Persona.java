package jenerics;

public class Persona {
    private int personId;
    private String name = "";

    public Persona() {
    }

    public Persona(int id) {
        super();
        this.personId = id;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "personId=" + personId +
                ", name='" + name + '\'' +
                '}';
    }
}

