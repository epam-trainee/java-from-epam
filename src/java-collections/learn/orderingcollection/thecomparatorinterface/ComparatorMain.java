package orderingcollection.thecomparatorinterface;

import orderingcollection.thecomparableinterface.Person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ComparatorMain {
    public static void main(String[] args) {
        ArrayList<Person> list = new ArrayList<>();
        list.add(new Person(41, "Bob"));
        list.add(new Person(57, "Jack"));
        list.add(new Person(56, "Jack"));
        list.add(new Person(17, "Bob"));
        list.add(new Person(10, "Jack"));
        list.add(new Person(19, "Bob"));
        System.out.println(list);
//      list.sort(new PersonNameComparator()); // (1)
//      Collections.sort(list, new PersonNameComparator()); // (2)
//        list.sort(new NameComparator()); // сортировка с использование статического внутреннего класса (3)
//        System.out.println(list); // результат в отсортированном виде
        list.sort(((o1, o2) -> o1.getPersonId() - o2.getPersonId())); // (4)
        System.out.println(list);
        list.sort(Comparator.comparing(Person::getName).thenComparing(Person::getPersonId)); // сортировка в другой сортировке (5)
        System.out.println(list);
    }
}
