package orderingcollection.thecomparableinterface;

public class Person implements Comparable<Person> {
    private int personId;
    private String name = "";

    public Person() {
    }

    public Person(int id) {
        super();
        this.personId = id;
    }

    public Person(String name) {
        this.name = name;
    }

    public Person(int personId, String name) {
        this.personId = personId;
        this.name = name;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "personId=" + personId +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(Person o) {
        int result = personId - o.personId; // сортировка по возрастанию Id
        int value = 0;
        if (result > 0) {
            value = 1;
        } else if (result < 0) {
            value = -1;
        }
        return value;
//        return name.compareTo(o); // сортировка по имени
//    }

    }
}
