package legacycollectionclasses;

import java.util.Enumeration;
import java.util.Hashtable;

public class LegacyMain {
    public static void main(String[] args) {
        Hashtable<String, Integer> table = new Hashtable<>();
        table.put("Jeans", 40);
        table.put("T-Shirt", 35);
        table.put("Gloves", 42);
        table.compute("Shoes", (k, v) -> 77);
        table.computeIfPresent("Shoes", (k, v) -> v + k.length()); // Shoes=82 увелилось на длину строки
        table.computeIfAbsent("Pants", v -> 11); // если элемент отсутствует то элемент будет добавлен
        System.out.println(table);
        Enumeration<String> keys = table.keys();
        while (keys.hasMoreElements()) {
            System.out.println(keys.nextElement());
        }
        Enumeration<Integer> values = table.elements();
        while ((values.hasMoreElements())) {
            System.out.println(values.nextElement());
        }
    }
}
