package legacycollectionclasses;

import java.util.Enumeration;
import java.util.Vector;

public class VectorMain {
    public static void main(String[] args) {
        Vector<String> vector = new Vector<>();
        vector.add("java");
        vector.add("Russia");
        vector.add(1, null);
        vector.addAll(vector);
        System.out.println(vector);
        vector.removeIf(e -> e == null); // удаление null
        vector.replaceAll(String::toUpperCase);
        System.out.println(vector); // JAVA, RUSSIA, JAVA, RUSSIA
        long size = vector.stream().count();
        System.out.println(size);
        Enumeration<String> enumeration = vector.elements();
        while ((enumeration.hasMoreElements())) {
            System.out.println(enumeration.nextElement());
        }
    }
}
