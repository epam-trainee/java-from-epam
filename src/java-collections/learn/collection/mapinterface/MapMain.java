import java.util.*;

public class MapMain {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("T-Shirt", 7);
        map.put("Jeans", 5);
        map.put("Gloves", 11);
        System.out.println(map);
        int value = map.put("Jeans", 4);
        System.out.println(map);
        System.out.println(value); // 5
        System.out.println();
        Set<String> set = map.keySet();
        System.out.println(set); // Gloves, T-Shirt, Jeans
        Collection<Integer> collection = map.values();
        System.out.println(collection); // 11, 7, 4
        System.out.println();
        Collection<Integer> collection2 = map.values();
        Set<Integer> sets = new HashSet<>(collection2);
        System.out.println(sets); // 4, 7, 11
        Set<Map.Entry<String, Integer>> values = map.entrySet();
        System.out.println(values); // Gloves=11, T-Shirt=7, Jeans=4

    }
}
