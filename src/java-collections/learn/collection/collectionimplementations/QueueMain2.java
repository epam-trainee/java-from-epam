package collectionimplementations;

import java.util.LinkedList;
import java.util.Queue;

public class QueueMain2 {
    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>() {
            {
                this.offer("Jeans");
            }
        };
        queue.add("Dress");
        queue.offer("T-Shirt");
//        queue.removeIf(s -> s.endsWith("t")); // удаление элемента с окончанием "t"
//        queue.forEach(System.out::println);
        System.out.println();
        // теперь тоже самое с использованием Stream
        queue.stream().filter(s -> !s.endsWith("t")).forEach(System.out::println);
        System.out.println();
        queue.forEach(System.out::println);

    }
}
