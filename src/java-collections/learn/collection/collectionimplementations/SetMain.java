package collectionimplementations;

import java.util.HashSet;
import java.util.Set;

public class SetMain {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("one");
        set.add("two");
        boolean value = set.add("ten");
        System.out.println(set + " " + value); // [one, ten, two] true
        value = set.add("ten"); // одинаковые элементы в коллекцию не добавляются
        System.out.println(set + " " + value); // [one, ten, two] false
    }
}
