package collectionimplementations;

import java.util.Comparator;
import java.util.Queue;
import java.util.PriorityQueue;

public class PriorityMain {
    public static void main(String[] args) {
        Queue<String> prior = new java.util.PriorityQueue<>(Comparator.reverseOrder());
        prior.offer("J");
        prior.offer("A");
        prior.offer("V");
        prior.offer("A");
        prior.offer("2");
        while (!prior.isEmpty()) {
            System.out.println(prior.poll()); // V J A A 2
        }
    }
}
