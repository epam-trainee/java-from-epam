package collectionimplementations;

import java.util.LinkedList;
import java.util.Queue;

public class QueueMain {
    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>() {
            {
                this.offer("Jeans");
            }
        };
        queue.add("Dress");
        queue.offer("T-Shirt");
        queue.forEach(System.out::println);
        System.out.println();
        queue.remove("Dress");
        queue.forEach(System.out::println); // Dress будет удален
        queue.clear();
        System.out.println();
        queue.element(); // проверка (examine) - NoSuchElementException
    }
}
