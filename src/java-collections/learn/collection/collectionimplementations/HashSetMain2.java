package collectionimplementations;

import java.util.HashSet;

public class HashSetMain2 {
    public static void main(String[] args) {
        HashSet<String> words = new HashSet<>(1000);
        words.add("8Y1");
        words.add("2Y");
        words.add("2Y2");
        words.add("8Y1");
        words.add("6Y");
        words.add("5Z");
        words.add("4Y5");
        System.out.println(words);
        for (String element : words) {
            System.out.print(element.hashCode() + " ");
        }
    }
}
