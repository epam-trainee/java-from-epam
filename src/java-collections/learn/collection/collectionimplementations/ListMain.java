package collectionimplementations;

import java.util.ArrayList;
import java.util.List;

public class ListMain {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(5);
        list.add(7);
        list.add(42);
        list.add(null);
        list.add(42);
        list.add(77);
        System.out.println(list);
        list.add(5, 87);
        System.out.println(list);
        Integer value = list.get(3); // получить значение элемента по индексу; оборачиваю в класс-обертку Integer, чтобы избежать NPE
        System.out.println(value); // null
        int index = list.indexOf(7);
        System.out.println(index); // 1
        list.remove(new Integer(42)); // обогачиваю в объект Integer чтобы избежать IndexOutOfBoundsException
        System.out.println(list);
        System.out.println(list.subList(1, 3)); // подсписок с 1 по 3 не включая
    }
}
