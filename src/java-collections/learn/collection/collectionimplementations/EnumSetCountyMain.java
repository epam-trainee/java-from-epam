package collectionimplementations;

import java.util.EnumSet;

public class EnumSetCountyMain {
    public static void main(String[] args) {
        EnumSet<Country> asiaCountries = EnumSet.of(Country.ARMENIA, Country.INDIA, Country.KAZAKHSTAN);
        String nameCounty = "India";
        Country current = Country.valueOf(nameCounty.toUpperCase());
        if (asiaCountries.contains(current)) {
            System.out.println("Asia Action");
        }
        asiaCountries.forEach(c -> c.grow(1));
    }
}
