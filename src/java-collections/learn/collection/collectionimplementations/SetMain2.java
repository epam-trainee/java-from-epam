package collectionimplementations;

import java.util.HashSet;
import java.util.Set;

public class SetMain2 {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>() {
            {
                this.add("one");
                this.add("two");
                this.add("ten");
            }
        };

        System.out.println(set); // добавление элементов через анонимный класс
    }
}

