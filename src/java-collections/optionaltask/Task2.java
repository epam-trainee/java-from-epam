// Ввести число, занести его цифры в стек. Вывести число, у которого цифры идут в обратном порядке.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Stack;

public class Task2 {
    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Пожалуйста, введите число");
            int size = Integer.parseInt(reader.readLine());
            Stack<Integer> stack = new Stack<>();
            Random random = new Random(size);
            int result = 0;
            for (int i = 0; i < size; i++) {
                result += random.nextInt(size) - size;
                stack.push(result);
            }
            System.out.println(stack);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Task2() {
        super();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
