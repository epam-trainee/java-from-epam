// Ввести строки из файла, записать в список ArrayList. Выполнить сортировку строк, используя метод sort() из класса Collections.

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;

public class Task6 {
    public static void main(String[] args) {
        try (BufferedReader lines = new BufferedReader(new InputStreamReader(new FileInputStream("C:\\Users\\Ольга\\Desktop\\out.txt"), "utf-8"))) {
            String line = lines.lines().collect(Collectors.joining(System.lineSeparator()));
            ArrayList<String> list = new ArrayList<>();
            new BufferedReader(new FileReader("C:\\Users\\Ольга\\Desktop\\out.txt")).lines().forEach(list::add);
            Collections.sort(list);
            System.out.println(line); // вывод текста
            System.out.println(list); // вывод массива
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

