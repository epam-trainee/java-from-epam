// Занести стихотворение в список. Провести сортировку по возрастанию длин строк.

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Task4 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        String verse = "Из под покрова тьмы ночной,\nИз черной ямы страшных мук\nБлагодарю я всех богов\nЗа свой непокоренный дух.";
        String[] words = verse.split("\n");
        list.add(String.valueOf(words));
        Arrays.sort(words);
        System.out.println(Arrays.toString(words));
    }
}
