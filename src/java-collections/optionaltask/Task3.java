// Создать список из элементов каталога и его подкаталогов

import java.io.File;

public class Task3 {
    public static void main(String[] args) {
        String s = null;
        File dir = new File("C:\\Program Files");
        if (dir.isDirectory()) {
            for (File item : dir.listFiles()) {
                if (item.isDirectory()) {
                    System.out.println(item.getName() + " catalog");
                } else {
                    System.out.println(item.getAbsolutePath() + " file");
                }
            }
        }
    }
}
