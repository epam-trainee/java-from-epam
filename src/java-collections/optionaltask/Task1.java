// Ввести строки из файла, записать в список. Вывести строки в файл в обратном порядке.

import java.io.*;

public class Task1 {
    public static void main(String[] args) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Users\\Ольга\\Desktop\\out.txt"));
            String text = bufferedReader.readLine();
            System.out.println(text); // текст 0 1 2 3 4 5 6 7 8 9
            System.out.println();
            StringBuilder builder = new StringBuilder(text);
            String result = String.valueOf(builder.reverse());
            System.out.println(result); // обратный текст 9 8 7 6 5 4 3 2 1 0

            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("C:\\Users\\Ольга\\Desktop\\in.txt"))) {
                if (result != null) {
                    bufferedWriter.write(result);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

