package task1;

import java.util.ArrayList;
import java.util.List;

import static task1.Bouquet.createBouquet;

public class ApplicationMain {
    public static void main(String[] args) {
        List<Flower> bouquet = createBouquet();
        // 1
        List<Flower> cheapBouquet = filterByPriceLowerThanAndFreshness(bouquet, 200.00);

        System.out.println("Собираем букет, где цена за букет будет ниже 200 руб. за цветок: " + cheapBouquet);
        System.out.println("Количество цветков в букете: " + cheapBouquet.size());
        System.out.println("Сумка за букет: " + totalPrice(cheapBouquet));

        // 2
        List<Flower> listByPriceAndFreshness = filterByPriceLowerThanAndFreshness(bouquet, 200.00, 7);
        System.out.println("Сортировка цветов в букете на основе его свежести: " + listByPriceAndFreshness);

        // 3
        List<Flower> listByPriceAndFreshnessByStem = filterByPriceAndFreshnessByStem(bouquet, 200.00, 7, 20);
        System.out.println("Цветок из букета в заданном диапазоне длины: " + listByPriceAndFreshnessByStem);
    }

    // 1
    static List<Flower> filterByPriceLowerThanAndFreshness(List<Flower> flowers, double price) {
        List<Flower> resultFilterByPrice = new ArrayList<>();
        for (Flower flower : flowers) {
            if (flower.price < price) {
                resultFilterByPrice.add(flower);
            }
        }
        return resultFilterByPrice;
    }

    // 1.1
    static double totalPrice(List<Flower> flowers) {

        double sum = 0;
        for (Flower s : flowers) {
            sum += s.price;
        }

        return sum;
    }

    // 2
    static List<Flower> filterByPriceLowerThanAndFreshness(List<Flower> flowers, double price, int freshness) {
        List<Flower> resultFilterByPriceAndFreshness = new ArrayList<>();
        for (Flower flower : flowers) {
            if ((flower.price < price) && (flower.freshness > freshness)) {
                resultFilterByPriceAndFreshness.add(flower);
            }
        }
        return resultFilterByPriceAndFreshness;
    }

    // 3
    static List<Flower> filterByPriceAndFreshnessByStem(List<Flower> flowers, double price, int freshness, int stem) {
        List<Flower> resultListByPriceAndFreshnessByStem = new ArrayList<>();
        for (Flower flower : flowers) {
            if ((flower.price < price) && (flower.freshness > freshness) && flower.stem > stem) {
                resultListByPriceAndFreshnessByStem.add(flower);
            }
        }
        return resultListByPriceAndFreshnessByStem;
    }
}


