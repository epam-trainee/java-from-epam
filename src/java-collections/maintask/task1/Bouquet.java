package task1;

import java.util.ArrayList;
import java.util.List;

class Bouquet {
    public static List<Flower> createBouquet() {
        List<task1.Flower> list = new ArrayList<>();
        list.add(new Flower("Роза", "красный", 120, 13, 9));
        list.add(new Flower("Роза", "белая", 180, 15, 8));
        list.add(new Flower("Роза", "желтая", 170, 21, 6));

        list.add(new Flower("Лилия", "15;0O", 190, 21, 8));
        list.add(new Flower("Тюльпан", "желтая", 150, 11, 3));
        return list;
    }
}

