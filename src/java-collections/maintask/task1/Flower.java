package task1;

public class Flower {
    String name;
    String color;
    double price;
    int stem;
    int freshness;

    public Flower(Lily lily) {

    }

    public Flower(Tulip tulip) {

    }

    public Flower(String name, String color, double price, int stem, int freshness) {
        this.name = name;
        this.color = color;
        this.price = price;
        this.stem = stem;
        this.freshness = freshness;
    }

    public Flower(Rose rose) {

    }

    static class Rose extends Flower {
        public Rose(String name, String color, double price, int stem, int freshness) {
            super(name, color, price, stem, freshness);
        }
    }

    static class Lily extends Flower {
        public Lily(String name, String color, double price, int stem, int freshness) {
            super(name, color, price, stem, freshness);
        }
    }

    static class Tulip extends Flower {
        public Tulip(String name, String color, double price, int stem, int freshness) {
            super(name, color, price, stem, freshness);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStem() {
        return stem;
    }

    public void setStem(int petal) {
        this.stem = petal;
    }

    public int getFreshness() {
        return freshness;
    }

    public void setFreshness(int freshness) {
        this.freshness = freshness;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", price=" + price +
                ", stem=" + stem +
                ", freshness=" + freshness +
                '}';
    }


}
