package maintask;
// Найти такое слово в первом предложении, которого нет ни в одном из остальных предложений.

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class Task3 {
    public static void main(String[] args) throws IOException {
        String s0 = "Lorem ipsum dolor sit amet ANOTHER_WORD";
        String s1 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";
        String s2 = Files.readAllLines(Paths.get("java-strings/maintask/text.txt")).get(0);

        Arrays.stream(s0.split("\\s+"))
                .filter(w -> !s1.concat(s2).contains(w))
                .forEach(System.out::println);
    }
}