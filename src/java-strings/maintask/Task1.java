package maintask;
// Найти наибольшее количество предложений текста, в которых есть одинаковые слова.

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Task1 {

    public static void main(String[] args) throws IOException {
        String s1 = Files.readAllLines(Paths.get("java-strings/maintask/text.txt")).get(0);
        Pattern p = Pattern.compile("nisi");
        Matcher m = p.matcher(s1);
        while (m.find())
            System.out.println("Совпадение: " + m.start() +
                    " to " + (m.end() - 1));
    }
}



