package maintask;
// Во всех вопросительных предложениях текста найти и напечатать без повторений слова заданной длины.

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task4 {
    public static void main(String[] args) throws IOException {
        String s1 = Files.readAllLines(Paths.get("java-strings/maintask/text.txt")).get(0);
        String lengthWord = String.valueOf(5);
        String[] sentences = s1.split("[\\.\\!\\?\\,]");

        System.out.println(Arrays.toString(sentences));

        Pattern p = Pattern.compile("\\?");
        Matcher m = p.matcher(Arrays.toString(sentences));
        while (m.find())
            System.out.println("Совпадение: " + m.start() +
                    " to " + (m.end() - 1));
    }
}

