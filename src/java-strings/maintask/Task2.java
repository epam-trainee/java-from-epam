package maintask;
// Вывести все предложения заданного текста в порядке возрастания количества слов в каждом из них.

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class Task2 {
    public static void main(String[] args) throws IOException {
        String s1 = Files.readAllLines(Paths.get("java-strings/maintask/text.txt")).get(0);
        String[] sentences = s1.split("[.;!?]");

        Arrays.sort(sentences);
        System.out.println("Sorted:");

        for (String s: sentences)
            System.out.println(s);
    }
}
