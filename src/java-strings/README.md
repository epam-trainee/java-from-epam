# EPAM java-strings
**Description**

Строка в языке Java — это основной носитель текстовой информации. Язык Java очень хорошо зарекомендовал себя при обработке информации, поэтому изучение возможностей классов их хранящих представляется необходимым. Системная библиотека Java содержит классы String, StringBuilder и StringBuffer, поддерживающие хранение строк, их обработку и определенные в пакете java.lang, подключаемом к приложению автоматически. Для форматирования и обработки строк применяются также классы Formatter, Pattern, Matcher, StringJoiner и другие.

**Course Goal**

Изучение основных приемов хранения и обработки информации.

**Agenda**

- String
- StringBuffer, StringBuilder
- Regular Expressions
- Pattern, Matcher
- Internazionalization
