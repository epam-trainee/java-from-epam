package learn.localization;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

public class ResourceBundleDemo {
    public static void main(String[] args) {
        Locale locale = new Locale("ru", "RU");
        ResourceBundle bundle = ResourceBundle.getBundle("res.message", locale);
        if (bundle.containsKey("str1")) {
            String s1 = bundle.getString("str1");
        }
        String s2 = bundle.getString("str2");
        Set<String> set = bundle.keySet();
        System.out.println(set);

    }
}
