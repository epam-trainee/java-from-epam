package learn.localization;

import java.util.Locale;

public class LocaleDemo {
    public static void main(String[] args) {
        Locale locale = Locale.FRANCE;
        Locale locale1 = Locale.getDefault();
        System.out.println(locale1);
        Locale locale2 = new Locale("be");
        Locale locale3 = new Locale("be", "BY");
        Locale.setDefault(locale2);
        System.out.println(locale.getCountry());
    }
}
