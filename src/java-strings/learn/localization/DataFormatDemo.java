package learn.localization;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

public class DataFormatDemo {
    public static void main(String[] args) {
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.MEDIUM, new Locale("ru", "RU"));
        String dateStr = dateFormat.format(new Date());
        System.out.println(dateStr);
        try {
            Date date = dateFormat.parse("четверг, 13 мая 2021 г., 21:10:35");
            System.out.println(date);
            DateFormat dataFormatCa = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, Locale.CANADA);
            System.out.println(dataFormatCa.format(date));
            DateFormat dataFormatGe = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG, Locale.GERMANY);
            System.out.println(dataFormatGe.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
