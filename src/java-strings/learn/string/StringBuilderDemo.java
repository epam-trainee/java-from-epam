package learn.string;

public class StringBuilderDemo {
    public static void main(String[] args) {
        StringBuilder builder = new StringBuilder();
        System.out.println(builder.length());
        System.out.println(builder.capacity());
        builder.append("hello");
        System.out.println(builder.length());
        System.out.println(builder.capacity());
        builder.insert(0, " internazinalization");
        System.out.println(builder.length());
        System.out.println(builder.capacity());
        System.out.println(builder.delete(10, 17));
        System.out.println(builder.reverse());
    }
}
