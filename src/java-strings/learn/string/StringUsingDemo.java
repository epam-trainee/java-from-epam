package learn.string;

import java.util.Arrays;
import java.util.List;

public class StringUsingDemo {
    static String str = "hello";

    public static void main(String[] args) {
        String str0 = new String("hello");
        System.out.println(str == str0); // false
        str0 = str0.intern();
        System.out.println(str == str0); // true
        String str1 = " ";
        System.out.println(str1.length()); // 1
        System.out.println(str1.isEmpty()); // false
        System.out.println(str1.isBlank()); // true

        String str2 = "   \n";
        str2 = "     667    78    ";
        System.out.println(str2.strip()); // trim() // 667    78
        System.out.println(str2.replaceAll("\\s+", "")); // 66778
        String xss = "<script> alert()</script>";
        xss.replaceAll("<?script>", "");
        str1 = "7876m.877?bvbv__-878789-Fghhf<><76";
        String [] strArr = str1.split("\\D+");
        System.out.println(Arrays.toString(strArr)); // 7876, 877, 878789, 76

        String str3 = String.format("java %.2s he%nllo %5.2f", "SE000", 55.987654321); // llo 55,99
        System.out.println(str3);

        String str4 = String.join("; ", List.of("a", "java", "12"));
        System.out.println(str4); // a; java; 12

    }
}
