package learn.string;

public class StringMain {
    static String str = "hello";

    public static void main(String[] args) {
        String str0 = "hello";
        System.out.println(str == str0); // true
        char[] ch = {'j', 'a', 'v', 'a'};
        System.out.println(new String(ch)); // java
        byte[] b = {48, 53, 65, 90, 97};
        System.out.println(new String(b)); // 05ZAa
        String str1 = "java" + 1 + Integer.parseInt("2");
        System.out.println(str1); // java12
        String str2 = 1 + Integer.parseInt("2") + "java" + false + null;
        System.out.println(str2); // 3javafalsenull
        str1 += str0;
        System.out.println(str1); // java12hello


    }
}
