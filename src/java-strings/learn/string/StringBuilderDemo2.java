package learn.string;

public class StringBuilderDemo2 {
    public static void main(String[] args) {
        StringBuilder builder1 = new StringBuilder();
        StringBuilder builder2 = new StringBuilder();
        builder1.append("java");
        builder2.append(builder1);
        System.out.println(builder1 + " " + builder2);
        System.out.println(builder1.equals(builder2) + " " + (builder1.hashCode() == builder2.hashCode())); // false false
        System.out.println(builder1.toString().contentEquals(builder2)); // true
    }
}
