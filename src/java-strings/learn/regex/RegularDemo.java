package learn.regex;

import java.util.Arrays;
import java.util.regex.Pattern;

public class RegularDemo {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("java");
        String[] res = pattern.split("dfjava12-- java5432", 2);
        System.out.println(Arrays.toString(res));
        //Pattern.matches("y+z", "yyz");
        System.out.println(Pattern.matches("y+z", "yyz")); // true
        System.out.println(Pattern.matches("y*z", "z")); // true
        System.out.println(Pattern.matches("y?z", "z")); // true
        System.out.println(Pattern.matches("y?z", "yyz")); // false


    }
}
