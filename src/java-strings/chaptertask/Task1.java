package chaptertask;
// В каждом слове текста k-ю букву заменить заданным символом. Если k больше длины слова, корректировку не выполнять.

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Task1 {
    public static void main(String[] args) throws IOException {
        String s = Files.readAllLines(Paths.get("java-strings/maintask/text.txt")).get(0);
        int k = 1;
        char symbol = '@';
        System.out.printf("Дано: %s%sРезультат: %s", s, System.lineSeparator(), replaceSymbol(s, k, symbol));
    }
        public static String replaceSymbol(String offer, int index, char symbol) {
            StringBuilder sb = new StringBuilder();
            if (offer != null && offer.length() > 0 && index > 0) {
                for (String word : offer.split(" ")) {
                    if (word.length() >= index) {
                        sb.append(word.substring(0, index - 1)).append(symbol).append(word.substring(index, word.length()));
                    } else {
                        sb.append(word);
                    }
                    sb.append(" ");
                }
            }
            return sb.toString();
        }
    }

