package learn.fileappenders;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogMain {
    static Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        logger.info("info first prg");
        logger.log(Level.WARN, "Warning ! Loggers !");
        try {
            throw new RuntimeException("Exception runtime");
        } catch (RuntimeException e) {
            logger.log(Level.ERROR, "first runtime", e);
        }
    }
}