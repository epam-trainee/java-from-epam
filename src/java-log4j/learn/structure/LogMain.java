package learn.structure;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.logging.LogManager;

public class LogMain {
    static org.apache.logging.log4j.Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        logger.error("first");
        logger.log(Level.ERROR, "second");
    }
}