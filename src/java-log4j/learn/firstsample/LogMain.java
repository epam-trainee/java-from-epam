package learn.firstsample;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogMain {
    static org.apache.logging.log4j.Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        // org.apache.logging.log4j.Logger logger = LogManager.getLogger(); // некорректное объявление логгера! Логгер всегда поле класса
        logger.info("info"); // не отобразится
        logger.error("first log");
    }
}
