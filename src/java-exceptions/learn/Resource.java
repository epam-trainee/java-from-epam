package learn;

public class Resource {
    public Resource() throws ResourceException{
        //
    }

    public boolean isCreate() {
        // more code
        return true;
    }

    public boolean exists() {
        // more code
        return false;
    }

    public void execute() {
        // more code
    }

    public void close() {
        // more code
    }
}
