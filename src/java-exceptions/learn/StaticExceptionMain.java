package learn;

public class StaticExceptionMain {
    final static int value;

    static {
        value = Integer.parseInt("Y-");
    }

    // ExceptionInInitializerError
    public static void main(String[] args) {
        int a = value;
    }
}
