package learn;

public class StoneService {
    public void buildHouse(Stone stone) {
        try {
            stone.accept("some info");
        } catch (ResourceException e) { // handling of ResourceException and its subclasses
            System.err.print(e);
        }
    }
}
