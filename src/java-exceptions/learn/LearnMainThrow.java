package learn;

public class LearnMainThrow {
    public static void main(String[] args) {
        int a = -42;
        if (a <= 0) {
            throw new IllegalArgumentException();
        }
        double res = Math.pow(a, 2);
    }
}
