package maintask;

import java.util.*;

public class Faculty {
    private String name; // имя студента
    private String facultyName;
    private int groupName;
    private String subject; // учебный предмет
    private int mark; // оценка за успеваемость
    private String subject2;
    private int mark2;

    public Faculty(String name, String facultyName, int groupName, String subject, int mark, String subject2, int mark2) {
        this.name = name;
        this.facultyName = facultyName;
        this.groupName = groupName;
        this.subject = subject;
        this.mark = mark;
        this.subject2 = subject2;
        this.mark2 = mark2;
    }

    public static void main(String[] args) {
        List<Faculty> list = createStudents();
        List<Faculty> StudentsAverageMarkBySubject = filterStudentsAverageMarkBySubject(list, "Марина");
        List<Faculty> StudentsBySubjectByGroupByFaculty = filterStudentsBySubjectByGroupByFaculty(list, "Аналитическая химия", 202, "Химический");
        List<Faculty> StudentsBySubjectAll = filterStudentsBySubjectAll(list, "Аналитическая химия");
    }

    private static List<Faculty> createStudents() {
        List<Faculty> list = new ArrayList<>();
        list.add(new Faculty("Марина", "Химический", 201, "Неорганическая химия", 8, "Органическая химия", 4));
        list.add(new Faculty("Александр", "Химический", 201, "Органическая химия", 8, "Неоргническая химия", 6));

        list.add(new Faculty("Артем", "Химический", 202, "Аналитическая химия", 9, "Физическая химия", 4));
        list.add(new Faculty("Олег", "Химический", 202, "Аналитическая химия", 10, "Полимерная химия", 12));

        list.add(new Faculty("Елена", "Естествознания", 301, "Русский язык", 7, "Литература", 10));
        list.add(new Faculty("Максим", "Естествознания", 301, "Обществознание", 8, "Русский язык", 10));

        list.add(new Faculty("Виктор", "Естествознания", 302, "История Отечества", 6, "Искусствоведение", 3));
        list.add(new Faculty("Вика", "Естествознания", 302, "Обществознание", 9, "История мира", 8));
        return list;
    }

    // Посчитать средний балл по всем предметам студента
    static List<Faculty> filterStudentsAverageMarkBySubject(List<Faculty> students, String name) {
        List<Faculty> resultStudentsAverageMarkBySubject = new ArrayList<>();
        for (Faculty f : students) {
            if (f.name.equals(name)) {
                int averageMarkByAllSubject = (f.mark + f.mark2) / 2;
                resultStudentsAverageMarkBySubject.add(f);
                System.out.println("Средний балл по всем предметам студента: " + averageMarkByAllSubject);
            }
        }
        return resultStudentsAverageMarkBySubject;
    }

    // Посчитать средний балл по конкретному предмету в конкретной группе и на конкретном факультете
    static List<Faculty> filterStudentsBySubjectByGroupByFaculty(List<Faculty> list, String subject, int groupName, String facultyName) {
        List<Faculty> resultStudentsBySubjectByGroupByFaculty = new ArrayList<>();
        int sumMarks = 0;
        int counter = 0;
        for (Faculty m : list) {
            if (m.subject.equals(subject) && m.groupName == groupName && m.facultyName.equals(facultyName)) {
                sumMarks += m.mark;
                counter++;
            }
        }
        System.out.println("Средний балл по конкретному предмету в конкретной группе и на конкретном факультете: " + sumMarks / counter);
        return resultStudentsBySubjectByGroupByFaculty;
    }

    // Посчитать средний балл по предмету для всего университета
    static List<Faculty> filterStudentsBySubjectAll(List<Faculty> list, String subject) {
        List<Faculty> resultStudentsBySubjectAll = new ArrayList<>();
        int sumMarkz = 0;
        int count = 0;
        for (Faculty v : list) {
            if (v.subject.equals(subject)) {
                sumMarkz += v.mark;
                count++;
            }
        }
        System.out.println("Средний балл по предмету для всего университета: " + sumMarkz / count);
        return resultStudentsBySubjectAll;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public int getGroupName() {
        return groupName;
    }

    public void setGroupName(int groupName) {
        this.groupName = groupName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String getSubject2() {
        return subject2;
    }

    public void setSubject2(String subject2) {
        this.subject2 = subject2;
    }

    public int getMark2() {
        return mark2;
    }

    public void setMark2(int mark2) {
        this.mark2 = mark2;
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "name='" + name + '\'' +
                ", facultyName='" + facultyName + '\'' +
                ", groupName=" + groupName +
                ", subject='" + subject + '\'' +
                ", mark=" + mark +
                ", subject2='" + subject2 + '\'' +
                ", mark2=" + mark2 +
                '}';
    }
}

