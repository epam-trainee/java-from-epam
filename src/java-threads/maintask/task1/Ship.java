package maintask.task1;

public class Ship {
    private final int liftingCapacity = 100_000; // грузоподъемность судна - 100 тонн
    private final int countShips = 5;
    private int countContainer = 5;

    public Ship(int countContainer) {
        this.countContainer = countContainer;
    }

    public int getLiftingCapacity() {
        return liftingCapacity;
    }

    public int getCountShips() {
        return countShips;
    }

    public int getCountContainer() {
        return countContainer;
    }

    public void setCountContainer(int countContainer) {
        this.countContainer = countContainer;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "liftingCapacity=" + liftingCapacity +
                ", countShips=" + countShips +
                ", countContainer=" + countContainer +
                '}';
    }
}
