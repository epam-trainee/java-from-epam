package maintask.task1;

import java.util.ArrayList;
import java.util.List;

public class Pier {
    private List<Ship> store;
    private static final int maxShipsInPier = 1;
    private static final int minShipsInPier = 0;
    private int shipsCounter = 0;

    public Pier() {
        store = new ArrayList<>();
    }

    public synchronized boolean add(Ship element) {
        try {
            if (shipsCounter < maxShipsInPier) {
                notifyAll();
                store.add(element);
                String information = String.format("The ship arrived on the pier: ", store.size(), shipsCounter, element.getLiftingCapacity(), Thread.currentThread().getName());
                System.out.println(information);
                shipsCounter++;

            } else {
                System.out.println(store.size() + "There is no place for a ship on the pier: " + Thread.currentThread().getName());
                wait();
                return false;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }


}
