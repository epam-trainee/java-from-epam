package maintask.task1;

public class ShipMain implements Runnable {

    private Pier pier;
    private int countShips;

    public ShipMain(Pier pier, int countShips) {
        this.pier = pier;
        this.countShips = countShips;
    }

    @Override
    public void run() {
        int count = 0;
        while (count < countShips) {
            Thread.currentThread().setName(" Generator ships");
            count++;
            pier.add(new Ship(5));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
