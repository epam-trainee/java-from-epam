package learn.threadsandexceptions;

import java.util.concurrent.TimeUnit;

public class ThreadException {
    public static void main(String[] args) {
        new Thread(() -> {
            boolean flag = true;
            if (flag) {
                throw new RuntimeException();
            }
            System.out.println("end of Exception Thread"); // появление Exception никак не влияет на выполнение других потоков
        }).start();
        try {
            TimeUnit.MILLISECONDS.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("end of main"); // end of main
    }
}

