package learn.timeunitandpriority;

import java.util.concurrent.TimeUnit;

public class TimeUnitDemo {
    public static void main(String[] args) {
        long timeout = 100 * 60 * (60 * 25 + 32);
        try {
            TimeUnit.MINUTES.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(timeout);
    }
}
