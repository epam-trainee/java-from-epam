package learn.timeunitandpriority;

import java.util.concurrent.TimeUnit;

public class TimeUnitDemo2 {
    public static void main(String[] args) {
        long timeout = 1000 * 60 * (60 * 25 + 32);
//        try {
//            Thread.sleep(1000 * 60 * (60 * 25 + 32)); // неудобен для записи и вычисления. Предпочтение лучше отдать TimeUnit
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        // TimeUnit.MINUTES.sleep(10);
        try {
            TimeUnit.MINUTES.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(timeout); // 91920000с = 25 часов 32 минуты
    }
}
