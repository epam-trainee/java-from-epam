package learn.timeunitandpriority;

public class PriorityDemo {
    public static void main(String[] args) {
        WalkThread walk = new WalkThread();
        Thread talk = new Thread(new TalkThread());
        talk.setPriority(Thread.MIN_PRIORITY); // 1
        walk.setPriority(Thread.MAX_PRIORITY); // 10
        talk.start();
        walk.start();
    }
}
