package learn.locks;

import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class PaymentLock {
    private int amount;
    private ReentrantLock lock = new ReentrantLock(true); // чтобы сформировать очередь из объектов
    private Condition condition = lock.newCondition();

    public void doPayment() {
        try {
            System.out.println("Start payment:");
            lock.lock(); // вместо synchronized
            while (amount <= 0) {
                condition.await(); // освобождаем поток и возвращаем блокировку Lock (вместо wait)
            }
            // payment here

            System.out.println("Payment is closed");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void init() {
        try {
            lock.lock();
            System.out.println("Init amount:");
            amount = new Scanner(System.in).nextInt();
        } finally {
            condition.signal(); // уведомляем что возможна разблокировка (вместо notify)
            lock.unlock();
        }
    }
}

