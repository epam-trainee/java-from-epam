package optionaltask;

public class Main {
    public static volatile Runway RUNWAY = new Runway();

    public static void main(String[] args) throws InterruptedException {
        for (int j = 1; j < 11; j++) {
            Plane plane = new Plane("Самолет " + j);
        }
    }

    public static class Plane extends Thread {
        public Plane(String name) {
            super(name);
            start();
        }

        public synchronized void run() {
            boolean isAlreadyTakenOff = false;
            while (!isAlreadyTakenOff) {
                if (RUNWAY.getTakingOffPlane() == null) {
                    RUNWAY.setTakingOffPlane(this);
                    System.out.println(getName() + " начал выход на полосу");
                    takingOff();
                    System.out.println(getName() + " взлетел");
                    isAlreadyTakenOff = true;
                    RUNWAY.setTakingOffPlane(null);
                } else if (!this.equals(RUNWAY.getTakingOffPlane())) {
                    System.out.println(getName() + " полоса освободилась");
                    waiting();
                }
            }
        }
    }

    private synchronized static void waiting() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }

    }

    private synchronized static void takingOff() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        waiting();
    }

    public static class Runway {
        private Thread t;

        public Thread getTakingOffPlane() {
            return t;
        }

        public void setTakingOffPlane(Thread t) {
            synchronized (this) {
                this.t = t;
            }
        }
    }
}

