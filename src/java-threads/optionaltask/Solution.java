package optionaltask;

import java.util.concurrent.*;

public class Solution {
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<String> queue = new LinkedBlockingQueue<>(5);
        // 1
        new Thread(() -> {
            for (int i = 1; i < 11; i++)
                try {
                    queue.offer("Самолет начал выход на полосу " + i);
                    System.out.println("Самолет " + i + " начал выход на полосу");
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        }).start();

        //2
        new Thread(() -> {

            try {
                for (int i = 1; i < 11; i++) {
                    System.out.println(queue.take() + " взлетел");
                    TimeUnit.SECONDS.sleep(2);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        // 3
        new Thread(() -> {
            try {
                for (int i = 1; i < 11; i++) {
                    queue.add("Полоса приняла самолет ");
                    System.out.println("Полоса приняла самолет " + i);
                    TimeUnit.SECONDS.sleep(1);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        // 4
        new Thread(() -> {

            try {
                for (int i = 1; i < 11; i++) {
                    TimeUnit.SECONDS.sleep(1);
                    System.out.println(queue.take());
                    System.out.println("Полоса освободилась");
                    TimeUnit.SECONDS.sleep(1);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}

